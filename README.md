# Product Registry

This is a test project for the junior developer position

## Prerequisites

* Apache Server with **mod_rewrite** enabled
* PHP ^7.4 (typed properties used)
* MySQL ^5.6 or MariaDB

## Installing

* Before launch the application, new database schema should be created. To do this, please run the following sql script:

```
<project_folder>/resources/schema.sql
```

* To populate data base with some initial data for testing, execute the following script:

```
<project_folder>/resources/initial-data.sql
```

* Copy <project_folder> into Apache Server  document root folder

* Change corresponding properties in [DATABASE] section of config file to mit you data base configuration:

```
<project_folder>/app/config/config.ini
```

* Now you can access application by the following url:

```
http://<host>/<project_folder>/ 
```

( by example - [http://localhost/product-registry/](http://localhost/product-registry/) )

#

2020, **Dmitry Kondratjev**
