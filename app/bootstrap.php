<?php

session_start();

define('DOC_ROOT', str_replace(['\\', $_SERVER['DOCUMENT_ROOT'], 'app'], ['/'], __DIR__));
define('CONFIG', parse_ini_file('config/config.ini'));

spl_autoload_register(function ($class) {
    $prefix = 'ProductRegistry';

    $base_dir = str_replace('/', '\\', __DIR__);
    $class_file = str_replace($prefix, $base_dir, $class) . '.php';

    if (file_exists($class_file)) {
        require $class_file;
    }
});
