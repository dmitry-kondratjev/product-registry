<?php

namespace ProductRegistry\controller;

use ProductRegistry\core\Controller;
use ProductRegistry\model\AttributeModel;

/**
 * Class AttributeController
 *
 * Provides functionality to process actions related to product attribute domain.
 *
 * @package ProductRegistry\controller
 */
class AttributeController extends Controller
{
    private AttributeModel $attributeModel;

    public function __construct()
    {
        parent::__construct();
        $this->attributeModel = new AttributeModel();
    }

    /**
     * Parses action parameter and call appropriated method.
     *
     * @param $action
     */
    public function doAction($action)
    {
        switch ($action) {
            case 'type-attributes':
                $this->getTypeAttributeList();
                break;
            default:
                $this->pageNotFound();
        }
    }

    /**
     * Returns list of attributes that belong to specified product type.
     */
    private function getTypeAttributeList()
    {
        $type_id = isset($_POST['type-id']) ? intval($_POST['type-id']) : 0;

        $attribute_list = $this->attributeModel->getTypeAttributeList($type_id);
        $this->sendJsonResponse($attribute_list);
    }
}
