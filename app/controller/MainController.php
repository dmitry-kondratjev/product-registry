<?php

namespace ProductRegistry\controller;

use ProductRegistry\core\Controller;

/**
 * Class MainController
 *
 * Controller that process requests to the root directory.
 *
 * @package ProductRegistry\controller
 */
class MainController extends Controller
{
    /**
     * Parses action parameter and call appropriated method.
     *
     * @param $action
     */
    public function doAction($action)
    {
        switch ($action) {
            case 'index':
                $this->mainPage();
                break;
            default:
                $this->pageNotFound();
        }
    }

    /**
     * Shows application's main page.
     */
    private function mainPage()
    {
        $this->view->generate('main.php');
    }
}
