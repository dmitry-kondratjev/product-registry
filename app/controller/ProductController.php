<?php

namespace ProductRegistry\controller;

use ProductRegistry\core\Controller;
use ProductRegistry\model\ProductModel;
use ProductRegistry\model\TypeModel;
use ProductRegistry\model\ValidationException;

/**
 * Class ProductController
 *
 * Provides functionality to process actions related to product domain.
 *
 * @package ProductRegistry\controller
 */
class ProductController extends Controller
{
    private ProductModel $productModel;

    public function __construct()
    {
        parent::__construct();
        $this->productModel = new ProductModel();
    }

    /**
     * Parses action parameter and call appropriated method.
     *
     * @param $action
     */
    public function doAction($action)
    {
        switch ($action) {
            case 'list':
                $this->getProductList();
                break;
            case 'new':
                $this->addNewProduct();
                break;
            case 'delete':
                $this->deleteProducts();
                break;
            case 'index':
                $this->redirectToProductList();
                break;
            default:
                $this->pageNotFound();
        }
    }

    /**
     * Returns list of products.
     */
    private function getProductList()
    {
        $product_list = $this->productModel->getProductList();
        $this->view->generate('product-list.php', $product_list);
    }

    /**
     * Saves new product in case of POST request,
     * otherwise returns product add form.
     */
    private function addNewProduct()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $product_data = json_decode(file_get_contents('php://input'), true);

            try {
                $this->productModel->saveProduct($product_data);
            } catch (ValidationException $e) {
                $this->sendJsonResponse([
                    'action' => 'message',
                    'message' => 'e_Error!_' . $e->getMessage()
                ]);
            }

            $_SESSION['message'] = 's_Message!_New product have been successfully saved.';
            $this->sendJsonResponse(['action' => 'reload']);
        }

        $typeModel = new TypeModel();
        $type_list = $typeModel->getTypeList();

        $this->view->generate('product-add.php', $type_list);
    }

    /**
     * Deletes all products by provided list of product id.
     */
    private function deleteProducts()
    {
        if (!isset($_POST['product-id-list'])) {
            $this->redirectToProductList();
        }

        $count = $this->productModel->deleteProducts($_POST['product-id-list']);

        if ($count > 0) {
            $_SESSION['message'] = "s_Message!_$count products have been successfully deleted.";
            $this->sendJsonResponse(['action' => 'reload']);
        } else {
            $this->sendJsonResponse([
                'action' => 'message',
                'message' => 'e_Error!_Some error occurred. No one product has been deleted.'
            ]);
        }
    }

    /**
     * Redirects request to product list url.
     */
    private function redirectToProductList()
    {
        header('Location: ' . DOC_ROOT . 'product/list');
        exit();
    }
}
