<?php

namespace ProductRegistry\core;

/**
 * Class Controller
 *
 * Abstract base class for controllers.
 * Provides some functionality common to all child controllers.
 *
 * @package ProductRegistry\core
 */
abstract class Controller
{
    protected View $view;

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * Abstract method to process user requested action.
     *
     * @param $action
     * @return mixed
     */
    abstract public function doAction($action);

    /**
     * Generates error page if requested controller or action was not found.
     */
    public function pageNotFound()
    {
        $this->view->generate('404.php');
    }

    /**
     * Sends JSON response to client.
     *
     * @param $response
     */
    public function sendJsonResponse($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }
}
