<?php

namespace ProductRegistry\core;

/**
 * Class Router
 *
 * This class is responsible for processing user requests.
 *
 * @package ProductRegistry\core
 */
class Router
{
    /**
     * Creates appropriated controller class and call his action method.
     */
    public function processRequest()
    {
        $prefix = 'ProductRegistry\\controller\\';
        $suffix = 'Controller';

        $request = $this->parseRequestUri();
        $controller_class = $prefix . $request['controller'] . $suffix;

        if (!class_exists($controller_class)) {
            $request['controller'] = 'Main';
            $request['action'] = 'page-not-found';
        }

        $controller = new $controller_class();
        $controller->doAction($request['action']);
    }

    /**
     * Determines controller name and action from request uri.
     *
     * @return array - associative array with 'controller' and 'action' keys
     */
    private function parseRequestUri(): array
    {
        $request = str_replace(DOC_ROOT, '', $_SERVER['REQUEST_URI']);
        $request = explode('/', $request);

        $result['controller'] = !empty($request[0]) ? ucfirst(strtolower($request[0])) : 'Main';
        $result['action'] = !empty($request[1]) ? strtolower($request[1]) : 'index';

        return $result;
    }
}
