<?php

namespace ProductRegistry\core;

/**
 * Class View
 *
 * @package ProductRegistry\core
 */
class View
{
    /**
     * Main html template that is common for all pages.
     */
    const TEMPLATE = '../app/view/template.php';

    /**
     * Generates html page from main template and provided content.
     *
     * @param string $content should be file name of included content.
     * @param null $data any data if it's necessary to render page content.
     */
    public function generate($content, $data = null)
    {
        require self::TEMPLATE;
    }
}
