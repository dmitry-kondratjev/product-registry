<?php

namespace ProductRegistry\dao;

use PDO;
use ProductRegistry\domain\Attribute;

/**
 * Class AttributeDAO
 *
 * Data Access Object to manipulate product attributes data.
 *
 * @package ProductRegistry\dao
 */
class AttributeDAO extends DAO
{
    /**
     * Returns list of product attributes that belong to specified type.
     *
     * @param int $type_id
     * @return array
     */
    public function getTypeAttributeList(int $type_id): array
    {
        $stmt = $this->pdo->prepare('
            SELECT
                id,
                name,
                description,
                pattern
            FROM
                attribute
                LEFT JOIN type_attribute ON id = attribute_id
            WHERE
                type_id = :type_id
        ');
        $stmt->execute([
            ':type_id' => $type_id
        ]);

        $rows = $stmt->fetchALL(PDO::FETCH_ASSOC);

        $attribute_list = [];

        foreach ($rows as $row) {
            $attribute = new Attribute();

            $attribute->setId($row['id']);
            $attribute->setName($row['name']);
            $attribute->setDescription($row['description']);
            $attribute->setPattern($row['pattern']);

            $attribute_list[] = $attribute;
        }

        return $attribute_list;
    }
}
