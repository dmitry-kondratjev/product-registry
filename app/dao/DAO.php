<?php

namespace ProductRegistry\dao;

use PDO;

/**
 * Class DAO
 *
 * Base class for data access objects. Provides $pdo object access database server.
 * Database configuration should be provided in app/config/config.ini file.
 *
 * @package ProductRegistry\dao
 */
class DAO
{
    protected PDO $pdo;

    public function __construct()
    {
        $dsn = 'mysql:host=' . CONFIG['db_host'] . ';port=' . CONFIG['db_port'] . ';dbname=' . CONFIG['db_name'];
        $this->pdo = new PDO($dsn, CONFIG['db_user'], CONFIG['db_pass']);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
