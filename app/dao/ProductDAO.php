<?php

namespace ProductRegistry\dao;

use PDO;
use ProductRegistry\domain\Attribute;
use ProductRegistry\domain\Product;

/**
 * Class ProductDAO
 *
 * Data Access Object to manipulate products data.
 *
 * @package ProductRegistry\dao
 */
class ProductDAO extends DAO
{
    /**
     * Returns list of products.
     *
     * @return array
     */
    public function getProductList(): array
    {
        $stmt = $this->pdo->query('
            SELECT
                p.id,
                p.sku,
                p.name,
                p.price,
                a.name as a_name,
                pa.value,
                a.unit
            FROM
                product p
                LEFT JOIN product_attribute pa ON p.id = pa.product_id
                LEFT JOIN attribute a ON a.id = pa.attribute_id
        ');
        $rows = $stmt->fetchALL(PDO::FETCH_ASSOC);

        $product_list = [];

        foreach ($rows as $row) {
            if (isset($product_list[$row['id']])) {
                $product = $product_list[$row['id']];
            } else {
                $product = new Product();
                $product->setId($row['id']);
                $product->setSku($row['sku']);
                $product->setName($row['name']);
                $product->setPrice($row['price']);
                $product_list[$row['id']] = $product;
            }

            $attribute = new Attribute();
            $attribute->setName($row['a_name']);
            $attribute->setValue($row['value']);
            $attribute->setUnit($row['unit']);
            $product->addAttribute($attribute);
        }

        return $product_list;
    }

    /**
     * Saves new product.
     *
     * @param Product $product
     */
    public function saveProduct(Product $product): void
    {
        $stmt = $this->pdo->prepare('
            INSERT INTO product (
                sku,
                name,
                price,
                type_id
            ) VALUES (
                :sku,
                :name,
                :price,
                :type_id
            )
        ');
        $stmt->execute([
            ':sku' => $product->getSku(),
            ':name' => $product->getName(),
            ':price' => $product->getPrice(),
            ':type_id' => $product->getType()->getId()
        ]);

        $product_id = $this->pdo->lastInsertId();

        foreach ($product->getAttributes() as $attribute) {
            $stmt = $this->pdo->prepare('
                INSERT INTO product_attribute (
                    product_id,
                    attribute_id,
                    value
                ) VALUES (
                    :product_id,
                    :attribute_id,
                    :value
                )
            ');
            $stmt->execute([
                ':product_id' => $product_id,
                ':attribute_id' => $attribute->getId(),
                ':value' => $attribute->getValue()
            ]);
        }
    }

    /**
     * Deletes all products by provided list of product id.
     * Returns number of deleted products.
     *
     * @param array $id_list
     * @return int
     */
    public function deleteProducts(array $id_list): int
    {
        return $this->pdo->exec('
            DELETE FROM 
                product
            WHERE
                id IN (' . implode(', ', array_map('intval', $id_list)) . ')
        ');
    }
}
