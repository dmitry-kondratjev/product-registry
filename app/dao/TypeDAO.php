<?php

namespace ProductRegistry\dao;

use PDO;
use ProductRegistry\domain\Type;

/**
 * Class TypeDAO
 *
 * Data Access Object to manipulate product type data.
 *
 * @package ProductRegistry\dao
 */
class TypeDAO extends DAO
{
    /**
     * Returns list of product types.
     *
     * @return array
     */
    public function getTypeList(): array
    {
        $stmt = $this->pdo->query('
            SELECT
                id,
                name
            FROM
                type
        ');
        $rows = $stmt->fetchALL(PDO::FETCH_ASSOC);

        $type_list = [];

        foreach ($rows as $row) {
            $type = new Type();

            $type->setId($row['id']);
            $type->setName($row['name']);

            $type_list[] = $type;
        }

        return $type_list;
    }
}
