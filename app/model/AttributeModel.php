<?php

namespace ProductRegistry\model;

use ProductRegistry\dao\AttributeDAO;

/**
 * Class AttributeModel
 *
 * @package ProductRegistry\model
 */
class AttributeModel
{
    private AttributeDAO $attributeDAO;

    public function __construct()
    {
        $this->attributeDAO = new AttributeDAO();
    }

    /**
     * Returns list of attributes that belong to particular product type.
     *
     * @param int $type_id
     * @return array
     */
    public function getTypeAttributeList(int $type_id): array
    {
        return $this->attributeDAO->getTypeAttributeList($type_id);
    }
}
