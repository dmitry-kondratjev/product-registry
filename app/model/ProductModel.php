<?php

namespace ProductRegistry\model;

use ProductRegistry\dao\ProductDAO;
use ProductRegistry\domain\Attribute;
use ProductRegistry\domain\Product;
use ProductRegistry\domain\Type;

/**
 * Class ProductModel
 *
 * @package ProductRegistry\model
 */
class ProductModel
{
    private ProductDAO $productDAO;

    public function __construct()
    {
        $this->productDAO = new ProductDAO();
    }

    /**
     * Returns product list.
     *
     * @return array
     */
    public function getProductList(): array
    {
        return $this->productDAO->getProductList();
    }

    /**
     * Saves new product if data validation is passed,
     * otherwise throw validation exception.
     *
     * @param array $product_data
     * @throws ValidationException
     */
    public function saveProduct(array $product_data)
    {
        $this->validateProductData($product_data);

        $product = $this->getProductObject($product_data);

        $this->productDAO->saveProduct($product);
    }

    /**
     * Deletes all products by provided list of product id.
     * Returns number of deleted products.
     *
     * @param array $id_list
     * @return int
     */
    public function deleteProducts(array $id_list): int
    {
        return $this->productDAO->deleteProducts($id_list);
    }

    /**
     * Validates product data.
     * May throw validation exception in case that data is not valid.
     *
     * @param array $data
     * @throws ValidationException
     */
    private function validateProductData(array $data)
    {
        if (!isset($data['sku']) || !strlen($data['sku'])) {
            throw new ValidationException("Product SKU not specified.");
        } elseif (strlen($data['sku']) < 3 || strlen($data['sku']) > 32) {
            throw new ValidationException("Product SKU should be at least 3 and no more than 32 chars.");
        }

        if (!isset($data['name']) || !strlen($data['name'])) {
            throw new ValidationException("Product name not specified.");
        } elseif (strlen($data['name']) > 50) {
            throw new ValidationException("Product name can't be longer than 50 chars.");
        }

        if (!isset($data['price']) || !strlen($data['price'])) {
            throw new ValidationException("Product price not specified.");
        } elseif (!is_numeric($data['price'])) {
            throw new ValidationException("Product price should be a number.");
        } elseif (floatval($data['price']) <= 0.00) {
            throw new ValidationException("Product price should be greater than zero.");
        }

        if (!isset($data['type_id']) || !is_numeric($data['type_id']) || intval($data['type_id']) < 1) {
            throw new ValidationException("Product type not specified.");
        }

        $type_id = intval($data['type_id']);
        $attributeModel = new AttributeModel();
        $type_attributes = $attributeModel->getTypeAttributeList($type_id);

        if (sizeof($type_attributes) > 0) {
            if (!isset($data['attributes']) || !is_array($data['attributes'])) {
                throw new ValidationException("Product attributes not specified.");
            }

            $product_attributes = [];
            foreach ($data['attributes'] as $a) {
                $product_attributes[$a[0]] = $a[1];
            }

            if (sizeof($product_attributes) > sizeof($type_attributes)) {
                throw new ValidationException("There are attributes that don't belong to this product type.");
            }

            foreach ($type_attributes as $t_attribute) {
                if (!isset($product_attributes[$t_attribute->getId()])) {
                    throw new ValidationException("Product attribute " . $t_attribute->getName() . " not specified.");
                }

                $p_attribute = $product_attributes[$t_attribute->getId()];

                if (!isset($p_attribute) || !strlen($p_attribute)) {
                    throw new ValidationException("Value of attribute " . $t_attribute->getName() . " not specified.");
                } elseif (!preg_match($t_attribute->getPattern(), $p_attribute)) {
                    throw new ValidationException("Value of attribute " . $t_attribute->getName() . " does not match specified format.");
                }
            }
        }
    }

    /**
     * Creates Product object from provided data
     * and sanitize user input.
     *
     * @param $data
     * @return Product
     */
    private function getProductObject($data): Product
    {
        $product = new Product();

        $product->setSku(htmlentities($data['sku']));
        $product->setName(htmlentities($data['name']));
        $product->setPrice(htmlentities($data['price']));

        $type = new Type();
        $type->setId(intval($data['type_id']));
        $product->setType($type);

        foreach ($data['attributes'] as $a) {
            $attribute = new Attribute();
            $attribute->setId(intval($a[0]));
            $attribute->setValue(htmlentities($a[1]));
            $product->addAttribute($attribute);
        }

        return $product;
    }
}
