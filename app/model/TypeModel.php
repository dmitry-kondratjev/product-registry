<?php

namespace ProductRegistry\model;

use ProductRegistry\dao\TypeDAO;

/**
 * Class TypeModel
 *
 * @package ProductRegistry\model
 */
class TypeModel
{
    private TypeDAO $typeDAO;

    public function __construct()
    {
        $this->typeDAO = new TypeDAO();
    }

    /**
     * Returns list of product type.
     *
     * @return array
     */
    public function getTypeList(): array
    {
        return $this->typeDAO->getTypeList();
    }
}
