<?php

namespace ProductRegistry\model;

use Exception;

/**
 * Class ValidationException
 *
 * Provides exception to indicate validation errors.
 *
 * @package ProductRegistry\model
 */
class ValidationException extends Exception
{
}
