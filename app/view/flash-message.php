<?php

if (isset($_SESSION['message'])):
    $message = explode("_", $_SESSION['message']);
    $type = $message[0];
    $title = $message[1];
    $text = $message[2];

    switch ($type) {
        case 's':
            $class = 'alert-success';
            break;
        case 'w':
            $class = "alert-warning";
            break;
        case 'e':
            $class = 'alert-danger';
            break;
        default:
            $class = 'alert-secondary';
    }
    ?>
    <div class="alert <?= $class ?> py-1 mb-2 alert-dismissible fade show">
        <button type="button" class="close py-1" data-dismiss="alert">&times;</button>
        <strong><?= $title ?></strong> <?= $text ?>
    </div>
    <?php
    unset($_SESSION['message']);
endif;
