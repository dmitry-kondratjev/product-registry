<div class="m-5 text-center">
    <h1>Product Registry</h1>
    <div class="m-5">
        <h5>This is a test project for the junior developer position</h5>
        <a href="https://www.dropbox.com/s/94dgq93l1tcpz7r/Junior%20Developer%20Test%20%20v2.4.pdf?dl=0">
            Link to assignment
        </a><br/>
        <p class="mt-3">2020, Dmitry Kondratjev</p>
    </div>
</div>
