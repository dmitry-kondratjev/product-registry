<div class="row border-bottom mb-3 justify-content-between">
    <div class="col-sm-auto">
        <h4 class="my-2">Product Add</h4>
    </div>
    <div class="col-sm-auto" id="message">
        <?php include 'flash-message.php'; ?>
    </div>
    <div class="col-sm-auto pb-2">
        <button type="button" class="btn btn-secondary ml-2" onclick="clearAddProductForm();">Clear form</button>
        <button type="button" class="btn btn-primary ml-2" onclick="saveNewProduct();">Save</button>
    </div>
</div>

<div class="row my-3">
    <div class="col p-0"></div>
    <div class="col-sm-auto">
        <div class="form-group row">
            <label for="sku" class="col-sm-2 col-form-label pr-0 text-sm-right">SKU:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="sku" size="50">
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label pr-0 text-sm-right">Name:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="name" size="50">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label pr-0 text-sm-right">Price:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="price" size="50">
            </div>
        </div>
        <div class="form-group row">
            <label for="type" class="col-sm-2 col-form-label pr-0 text-sm-right">Type:</label>
            <div class="col-auto">
                <select class="custom-select" id="type" onchange="getProductAttributes();">
                    <option value="-1" selected>Select product type ...</option>
                    <?php foreach ($data as $type): ?>
                        <option value="<?= $type->getId() ?>"><?= $type->getName() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div id="product-attributes">
        </div>
        <div class="form-group row">
            <div class="col-sm-2" style="width: 25rem;"></div>
            <div class="col-sm-9">
                <p class="text-info mt-2">* All fields are required</p>
            </div>
        </div>
    </div>
    <div class="col p-0"></div>
</div>
