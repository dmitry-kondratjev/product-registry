<div class="row border-bottom mb-3 justify-content-between">
    <div class="col-sm-auto">
        <h4 class="my-2">Product List</h4>
    </div>
    <div class="col-sm-auto" id="message">
        <?php include 'flash-message.php'; ?>
    </div>
    <div class="col-sm-auto pb-2">
        <div class="form-inline">
            <select class="custom-select w-auto" id="list-action">
                <option selected>Select action ...</option>
                <option value="delete">Mass Delete</option>
            </select>
            <button type="button" class="btn btn-primary ml-2" onclick="productListAction();">Apply</button>
        </div>
    </div>
</div>

<div class="card-deck mb-3">
    <?php foreach ($data as $product): ?>
        <div class="card my-2 card-width">
            <div class="card-body text-center">
                <input type="checkbox" class="position-absolute" name="product-card" value="<?= $product->getId(); ?>">
                <h6 class="card-title"><?= $product->getSku(); ?></h6>
                <h5 class="card-title"><?= $product->getName(); ?></h5>
                <p class="card-text"><?= $product->getPrice(); ?> EUR</p>
                <?php foreach ($product->getAttributes() as $attribute): ?>
                    <p class="card-text mb-1"><?= $attribute->getName() . ': ' . $attribute->getValue() . ' ' . $attribute->getUnit(); ?></p>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
