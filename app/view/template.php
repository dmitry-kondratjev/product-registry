<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Product Registry</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= DOC_ROOT ?>css/style.css">
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-primary mb-3">
    <a href="<?= DOC_ROOT ?>" class="navbar-brand pt-0">Product Registry</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse border-left border-left-xs-none" id="navbar-collapse">
        <div class="navbar-nav">
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Products</a>
                <div class="dropdown-menu">
                    <a href="<?= DOC_ROOT ?>product/list" class="dropdown-item">Product list</a>
                    <a href="<?= DOC_ROOT ?>product/new" class="dropdown-item">Add new product</a>
                </div>
            </div>
            <!--<a href="#" class="nav-item nav-link">Link 1</a>
            <a href="#" class="nav-item nav-link active">Link 2</a>-->
        </div>
    </div>
</nav>

<div class="container-fluid">
    <?php include $content; ?>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script>
    const doc_root = "<?= DOC_ROOT ?>";
</script>
<script src="<?= DOC_ROOT ?>js/main.js"></script>
</body>
</html>
