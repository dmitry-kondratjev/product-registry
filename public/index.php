<?php

use ProductRegistry\core\Router;

require '../app/bootstrap.php';

$router = new Router();
$router->processRequest();
