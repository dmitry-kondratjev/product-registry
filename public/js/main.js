/*
    Main utility script for Product Registry application.
*/

function productListAction() {
    switch ($("#list-action").val()) {
        case "delete":
            deleteSelectedProducts();
            break;
        default:
            showMessage("w_Warning!_Please select an action.");
    }
}

function deleteSelectedProducts() {
    let id_list = [];
    $("[name='product-card']:checked").each(function () {
        id_list.push($(this).val());
    });

    if (id_list.length === 0) {
        showMessage("w_Warning!_There is no one product selected.");
        return;
    }

    $.post(doc_root + "product/delete", {"product-id-list": id_list}, function (response) {
        processServerResponse(response);
    });
}

function getProductAttributes() {
    $("#product-attributes").empty();

    let type_id = $("#type").val();
    if (type_id < 0) return;

    $.post(doc_root + "attribute/type-attributes", {"type-id": type_id}, function (attributes) {
        showProductAttributes(attributes);
    });
}

function showProductAttributes(attributes) {
    attributes.forEach(function (attribute) {
        $("#product-attributes").append($(
            '<div class="form-group row">' +
            '    <label for="' + attribute.id + '" class="col-sm-2 col-form-label pr-0 text-sm-right">' + attribute.name + ':</label>' +
            '    <div class="col-sm-9">' +
            '        <input type="text" class="form-control" name="prod-attr" id="' + attribute.id + '" size="50">' +
            '        <p class="m-0 font-italic">' + attribute.description + '</p>' +
            '    </div>' +
            '</div>'
        ));
    });
}

function clearAddProductForm() {
    $("#sku").val("");
    $("#name").val("");
    $("#price").val("");
    $("[name=prod-attr]").val("");
}

function saveNewProduct() {
    let attributes = [];
    $("[name='prod-attr']").each(function () {
        attributes.push([$(this).attr("id"), $(this).val()]);
    });

    let product_data = {
        "sku": $("#sku").val(),
        "name": $("#name").val(),
        "price": $("#price").val(),
        "type_id": $("#type").val(),
        "attributes": attributes
    };

    //if (!validateProductData(product_data)) return;

    $.post(doc_root + "product/new", JSON.stringify(product_data), function (response) {
        processServerResponse(response);
    });
}

/*
    Basic validation of new product data.
    The main validation is performed on the server side.
*/
function validateProductData(product_data) {
    if (product_data.sku === "") {
        showMessage("w_Warning!_Field <strong>SKU</strong> is required.");
        return false;
    }

    if (product_data.name === "") {
        showMessage("w_Warning!_Field <strong>Name</strong> is required.");
        return false;
    }

    if (product_data.price === "") {
        showMessage("w_Warning!_Field <strong>Price</strong> is required.")
        return false;
    } else if (isNaN(parseFloat(product_data.price))) {
        showMessage("w_Warning!_<strong>Price</strong> should be a number.")
        return false;
    }

    if (product_data.type_id < 1) {
        showMessage("w_Warming!_Product type not selected.");
        return false;
    }

    for (let attribute of product_data.attributes) {
        if (attribute[1] === "") {
            let field = $("label[for='" + attribute[0] + "']").text().slice(0, -1);
            showMessage("w_Warning!_Field <strong>" + field + "</strong> is required.");
            return false;
        }
    }

    return true;
}

// Process server response and perform appropriated action
function processServerResponse(response) {
    switch (response.action) {
        case "reload":
            location.reload();
            break;
        case "message":
            showMessage(response.message);
            break;
    }
}

function showMessage(message) {
    let m = message.split("_");
    let type = m[0];
    let title = m[1];
    let text = m[2];

    let message_class;
    switch (type) {
        case "s":
            message_class = "alert-success";
            break;
        case "w":
            message_class = "alert-warning";
            break;
        case "e":
            message_class = "alert-danger";
            break;
        default:
            message_class = "secondary";
    }

    $("#message").empty().append($(
        '<div class="alert ' + message_class + ' py-1 mb-2 alert-dismissible fade show">' +
        '    <button type="button" class="close py-1" data-dismiss="alert">&times;</button>' +
        '    <strong>' + title + '</strong> ' + text +
        '</div>'
    ));
}
