DROP DATABASE IF EXISTS product_registry;
CREATE DATABASE product_registry DEFAULT CHARACTER SET utf8;

GRANT ALL ON product_registry.* TO 'preg'@'localhost' IDENTIFIED BY 'preg';
GRANT ALL ON product_registry.* TO 'preg'@'127.0.0.1' IDENTIFIED BY 'preg';

USE product_registry;

CREATE TABLE type (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(32) NOT NULL,

    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE attribute (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(32) NOT NULL,
    unit VARCHAR(10) NOT NULL,
    description VARCHAR(200) NOT NULL,
    pattern VARCHAR(50) NOT NULL DEFAULT '/.+/',

    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE type_attribute (
    type_id INT UNSIGNED NOT NULL,
    attribute_id INT UNSIGNED NOT NULL,

    PRIMARY KEY (type_id, attribute_id),
    FOREIGN KEY (type_id)
        REFERENCES type (id) ON DELETE CASCADE,
    FOREIGN KEY (attribute_id)
        REFERENCES attribute (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE product (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    sku VARCHAR(32) NOT NULL,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(9,2) NOT NULL,
    type_id INT UNSIGNED NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (sku),
    FOREIGN KEY (type_id)
        REFERENCES type (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE product_attribute (
    product_id INT UNSIGNED NOT NULL,
    attribute_id INT UNSIGNED NOT NULL,
    value VARCHAR(50) NOT NULL,

    PRIMARY KEY (product_id, attribute_id),
    FOREIGN KEY (product_id)
        REFERENCES product (id) ON DELETE CASCADE,
    FOREIGN KEY (attribute_id)
        REFERENCES attribute (id) ON DELETE CASCADE
) ENGINE = InnoDB;
